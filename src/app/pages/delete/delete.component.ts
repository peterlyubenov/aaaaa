import { Component, OnInit } from '@angular/core';
import Watch from '../../models/Watch';
import { WatchService } from '../../services/watch.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  public id: number;
  public watch: Watch;

  constructor(private watchService: WatchService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      
      this.watchService.getById(this.id).subscribe((watch: Watch) => {
        this.watch = watch;
      })
    })
  }

  public submitForm(event): void {
    // Prevent form submission over HTTP
    event.preventDefault();

    this.watchService.delete(this.id).subscribe(() => {
      this.router.navigate(['/home']);
    })
  }
}
