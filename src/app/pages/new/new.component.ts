import { Component, OnInit } from '@angular/core';
import { WatchService } from '../../services/watch.service';
import Brands from '../../models/BrandsCollection';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {

  public brands: string[];

  constructor(private watchService: WatchService, private router: Router) { }

  ngOnInit(): void {
    this.watchService.getBrands().subscribe((data: Brands) => {
      this.brands = data.brands;
    })
  }

  public submitForm(event, brand, display, style): void {
    // Prevent form submission over HTTP
    event.preventDefault();

    // Validate all values are required
    if (brand.value.length === 0 || display.value.length === 0 || style.value.length === 0) return;

    this.watchService.create({ brand: brand.value, display: display.value, style: style.value }).subscribe(() => {
      this.router.navigate(['/home']);
    })
  }
}
