import { Component, OnInit } from '@angular/core';
import Watch from '../../models/Watch';
import { WatchService } from '../../services/watch.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public watchService: WatchService) { }

  public watches: Watch[];

  ngOnInit(): void {
    this.watchService.getLatest().subscribe((data: Watch[]) => {
      this.watches = data;
    })
  }

}
