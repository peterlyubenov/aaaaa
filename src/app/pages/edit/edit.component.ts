import { Component, OnInit } from '@angular/core';
import Brands from '../../models/BrandsCollection';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { WatchService } from '../../services/watch.service';
import Watch from '../../models/Watch';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  public brands: string[];
  public id: number;
  public watch: Watch;

  constructor(private watchService: WatchService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      
      this.watchService.getById(this.id).subscribe((watch: Watch) => {
        this.watch = watch;
      })
    })

    this.watchService.getBrands().subscribe((data: Brands) => {
      this.brands = data.brands;
    })
  }

  public submitForm(event, brand, display, style): void {
    // Prevent form submission over HTTP
    event.preventDefault();

    // Validate all values are required
    if (brand.value.length === 0 || display.value.length === 0 || style.value.length === 0) return;

    this.watchService.update(this.id, { brand: brand.value, display: display.value, style: style.value }).subscribe(() => {
      this.router.navigate(['/home']);
    })
  }
}
