import { Component, OnInit, Input } from '@angular/core';
import Watch from '../../models/Watch';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  constructor() { }

  @Input() watch: Watch;

  ngOnInit(): void {
  }

}
