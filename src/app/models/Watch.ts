export default interface Watch {
  brand: string,
  display: string,
  style: string,
  id?: number
}