import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Brands from '../models/BrandsCollection';
import Watch from '../models/Watch';


const api = "http://localhost:3000";
const url = (path: string): string => api + path;

@Injectable({
  providedIn: 'root'
})
export class WatchService {

  constructor(private http: HttpClient) { }

  public getBrands(): Observable<Brands> {
    return this.http.get<Brands>(url('/brands'));
  }

  public getLatest(): Observable<Watch[]> {
    return this.http.get<Watch[]>(url('/watches?_sort=id&_order=desc&_limit=8'));
  }

  public getById(id: number): Observable<Watch> {
    return this.http.get<Watch>(url('/watches/' + id));
  }

  public create(watch: Watch) {
    console.log(watch);
    return this.http.post(url('/watches'), watch);
  }

  public update(id: number, watch: Watch) {
    return this.http.put(url('/watches/' + id), watch);
  }

  public delete(id: number) {
    return this.http.delete(url('/watches/' + id));
  }

}
